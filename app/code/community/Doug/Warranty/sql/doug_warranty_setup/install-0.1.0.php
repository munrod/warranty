<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$installer->run("
 DROP TABLE IF EXISTS `{$installer->getTable('doug_warranty')}`;
CREATE TABLE `{$installer->getTable('doug_warranty')}` (
  `id` int(11) NOT NULL auto_increment,
  `item_type` varchar(32) NOT NULL default 'small_item',
  `years` smallint(8) unsigned  default '0',
  `price_min` decimal(12,4) NOT NULL default '0.0000',
  `price_max` decimal(12,4) NOT NULL default '0.0000',
  `price` decimal(12,4) NOT NULL default '0.0000',
  `created_at` datetime NOT NULL default '0000-00-00 00:00:00',
  `expire_at` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();

