<?php
$this->startSetup();
Mage::register('isSecureArea', 1);

Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);


$rootPath = '1/2';
$categoryData = array(
    array(
        'path'    => '1/2',
        'name'    => 'Small item',
        'url_key' => 'small-item'
    ),
    array(
        'path'    => '1/2',
        'name'    => 'Large item',
        'url_key' => 'large-item'
    )
);

$category = Mage::getModel('catalog/category');
foreach ($categoryData as $item) {
    $category->setPath($item['path'])
        ->setName($item['name'])
        ->setUrlKey($item['url_key'])
        ->setIsActive(1)
        ->setIncludeInMenu(0)
        ->save();
    $category->unsetData('entity_id');
}

$this->endSetup();
