<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$warrantyItem = array(
    array(
        'item_type' => 'small_item',
        'years' => 1,
        'price_min' => 0,
        'price_max' => 100,
        'price' => 50
    ),
    array(
        'item_type' => 'small_item',
        'years' => 1,
        'price_min' => 100,
        'price_max' => 500,
        'price' => 60
    ),
    array(
        'item_type' => 'small_item',
        'years' => 1,
        'price_min' => 500,
        'price_max' => PHP_INT_MAX,
        'price' => 70
    ),
    array(
        'item_type' => 'small_item',
        'years' => 2,
        'price_min' => 0,
        'price_max' => 100,
        'price' => 80
    ),
    array(
        'item_type' => 'small_item',
        'years' => 2,
        'price_min' => 100,
        'price_max' => 500,
        'price' => 90
    ),
    array(
        'item_type' => 'small_item',
        'years' => 2,
        'price_min' => 500,
        'price_max' => PHP_INT_MAX,
        'price' => 100
    ),
    array(
        'item_type' => 'small_item',
        'years' => 3,
        'price_min' => 0,
        'price_max' => 100,
        'price' => 110
    ),
    array(
        'item_type' => 'small_item',
        'years' => 3,
        'price_min' => 100,
        'price_max' => 500,
        'price' => 120
    ),
    array(
        'item_type' => 'small_item',
        'years' => 3,
        'price_min' => 500,
        'price_max' => PHP_INT_MAX,
        'price' => 130
    ),
    array(
        'item_type' => 'large_item',
        'years' => 1,
        'price_min' => 0,
        'price_max' => 100,
        'price' => 55
    ),
    array(
        'item_type' => 'large_item',
        'years' => 1,
        'price_min' => 100,
        'price_max' => 500,
        'price' => 65
    ),
    array(
        'item_type' => 'large_item',
        'years' => 1,
        'price_min' => 500,
        'price_max' => PHP_INT_MAX,
        'price' => 75
    ),

    array(
        'item_type' => 'large_item',
        'years' => 2,
        'price_min' => 0,
        'price_max' => 100,
        'price' => 85
    ),
    array(
        'item_type' => 'large_item',
        'years' => 2,
        'price_min' => 100,
        'price_max' => 500,
        'price' => 95
    ),
    array(
        'item_type' => 'large_item',
        'years' => 2,
        'price_min' => 500,
        'price_max' => PHP_INT_MAX,
        'price' => 105
    ),

    array(
        'item_type' => 'large_item',
        'years' => 3,
        'price_min' => 0,
        'price_max' => 100,
        'price' => 115
    ),
    array(
        'item_type' => 'large_item',
        'years' => 3,
        'price_min' => 100,
        'price_max' => 500,
        'price' => 125
    ),
    array(
        'item_type' => 'large_item',
        'years' => 3,
        'price_min' => 500,
        'price_max' => PHP_INT_MAX,
        'price' => 135
    ),

);

foreach ($warrantyItem as $warranty) {
    Mage::getModel('doug_warranty/warranty')
        ->setData($warranty)
        ->save();
}

$installer->endSetup();
