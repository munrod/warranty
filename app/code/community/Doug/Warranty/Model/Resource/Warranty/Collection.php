<?php
class Doug_Warranty_Model_Resource_Warranty_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('doug_warranty/warranty');
    }
}
