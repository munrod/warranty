<?php

class Doug_Warranty_Model_Resource_Warranty extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('doug_warranty/warranty', 'id');
    }
}
