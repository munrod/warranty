<?php

class Doug_Warranty_Model_Catalog_Product_Option_Value extends Mage_Catalog_Model_Product_Option_Value {

    private $price;

    private $productPrice;

    public function getPrice($flag=false)
    {
        if ($flag && $this->getPriceType() == 'percent') {
            $basePrice = $this->getOption()->getProduct()->getFinalPrice();
            $price = $basePrice*($this->_getData('price')/100);
            return $price;
        }
        $this->productPrice = $this->getProduct()->getPrice();
        $this->price = $this->_getData('price');
        $data = array( 'option' => $this);
        Mage::dispatchEvent('catalog_model_option_price', $data);

        return $this->price;
    }

    public function getOptionPrice(){
        return $this->price;
    }

    public function setOptionPrice($price){
        $this->price = $price;
    }
}
