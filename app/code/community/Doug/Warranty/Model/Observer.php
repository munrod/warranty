<?php

class Doug_Warranty_Model_Observer
{
    private $skuTypes = array('0_year_warranty', '1_year_warranty', '2_year_warranty', '3_year_warranty');

    /**
     * @param Varien_Event_Observer $event
     */
    public function catalogModelOptionPrice(Varien_Event_Observer $event)
    {
        $this->alterWarrantyPrice($event);
    }

    /**
     * @param $event
     * @return bool
     */
    private function alterWarrantyPrice($event)
    {
        $option = $event['option'];
        $optionSku = $option->getSku();

        if ( ! $this->isValidSkuType($optionSku) || ! $option->getProduct() ) {
            return false;
        }

        $product = $option->getProduct();

        $newOptionPrice = $this->getOptionPrice($optionSku, $product);

        $this->alterPrice($option, $newOptionPrice);

    }

    /**
     * @param $sku
     * @return bool
     */
    private function isValidSkuType($sku)
    {
        return in_array($sku, $this->skuTypes);
    }

    /**
     * @param $optionSku
     * @param $product
     * @return int
     */
    private function getOptionPrice($optionSku, $product)
    {
        $price = 0;
        if ( $this->isSmallItem($product->getId()) ) {
            $price = $this->getPriceForItemTypes('small_item', $optionSku, $product);
        }

        if ( $this->isLargeItem($product->getId()) ) {
            $price = $this->getPriceForItemTypes('large_item', $optionSku, $product);
        }
        return $price;
    }

    /**
     * @param $type
     * @param $optionSku
     * @param $product
     * @return int
     */
    private function getPriceForItemTypes($type, $optionSku, $product)
    {
        list($year, $text, $warranty) = explode('_', $optionSku);
        $newPrice = 0;

        $warrantyPrice = $this->getWarrantyPrice($type, $year, $product->getPrice());
        foreach ($warrantyPrice as $item) {
            $newPrice = $item->getPrice();
        }
        return $newPrice;
    }

    /**
     * @param $item_type
     * @param $year
     * @param $productPrice
     * @return Doug_Warranty_Model_Resource_Warranty_Collection
     */
    private function getWarrantyPrice($item_type, $year, $productPrice)
    {
        /** @var  $collection Doug_Warranty_Model_Resource_Warranty_Collection */
        $collection = Mage::getModel('doug_warranty/warranty')->getCollection();
        $collection->addFieldToSelect('price')
            ->addFieldToFilter('item_type', $item_type)
            ->addFieldToFilter('years', $year)
            ->addFieldToFilter('price_min', array('lt' => $productPrice))
            ->addFieldToFilter('price_max', array('gteq' => $productPrice))
            ->getFirstItem();

        return $collection;
    }

    /**
     * @param $productId
     * @return int
     */
    private function isSmallItem($productId)
    {
        return $this->getWarrantyTypeData($this->getSmallItemCategory(), $productId)->getSize();
    }

    /**
     * @param $productId
     * @return int
     */
    private function isLargeItem($productId)
    {
        return $this->getWarrantyTypeData($this->getLargeItemCategory(), $productId)->getSize();
    }

    /**
     * @param $category
     * @param $productId
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    private function getWarrantyTypeData($category, $productId)
    {
        /** @var  $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection
            ->addAttributeToSelect('price')
            ->addAttributeToFilter('entity_id', $productId)
            ->addCategoryFilter($category)
            ->getFirstItem();

        return $collection;
    }


    /**
     * @return bool|Mage_Catalog_Model_Abstract
     */
    private function getSmallItemCategory()
    {
        return Mage::getModel('catalog/category')->loadByAttribute('url_key', 'small-item');
    }

    /**
     * @return bool|Mage_Catalog_Model_Abstract
     */
    private function getLargeItemCategory()
    {
        return Mage::getModel('catalog/category')->loadByAttribute('url_key', 'large-item');
    }


    /**
     * @param $option
     * @param $price
     */
    private function alterPrice($option, $price)
    {
        $option->setOptionPrice($price);
    }

}
