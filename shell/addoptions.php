<?php
require_once 'abstract.php';

class Mage_Shell_Addoptions extends Mage_Shell_Abstract
{
private $itemCount = 0;
    /**
     * Run script
     *
     */
    public function run()
    {
        $this->createProductCustomOptions();
    }

    private function createProductCustomOptions()
    {
        echo date('h:i:s');
        echo PHP_EOL;

        $this->createOptionsItems($this->getSmallItemCategory());
        $this->createOptionsItems($this->getLargeItemCategory());

        echo date('h:i:s');
        echo PHP_EOL;
        echo 'product saved: ' . $this->itemCount . PHP_EOL;
    }

    private function createOptionsItems($categoryType)
    {
        /** @var  $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getModel('catalog/product')->getCollection();

        $collection
            ->addAttributeToSelect('price')
            ->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
            ->addCategoryFilter($categoryType);

        var_dump($collection->getSize());

        foreach ($collection as $product) {
            $optionInstance = $product->getOptionInstance()->unsetOptions();
            $product->setHasOptions(1);
            $optionInstance->addOption($this->getOptionsData());
            $optionInstance->setProduct($product);

            $product->setUrlKey(false);

            if ( $product->save() ) {
                echo 'product saved id: ' . $product->getId() . PHP_EOL;
            }
            $this->itemCount ++;
        }
    }

    private function getOptionsData()
    {
        return  array(
            'title'      => 'Warranty',
            'type'       => 'drop_down',
            'is_require' => 1,
            'sort_order' => 0,
            'values'     => $this->getOptions()
        );
    }

    private function getSmallItemCategory()
    {
        return Mage::getModel('catalog/category')->loadByAttribute('url_key','small-item');
    }

    private function getLargeItemCategory()
    {
        return Mage::getModel('catalog/category')->loadByAttribute('url_key','large-item');
    }

    private function getOptions()
    {
        return array(
            array(
                'title'      => 'No thanks',
                'price'      => 0,
                'price_type' => 'fixed',
                'sku'        => '0_year_warranty',
                'sort_order' => '0'
            ),
            array(
                'title'      => '1 year',
                'price'      => 0,
                'price_type' => 'fixed',
                'sku'        => '1_year_warranty',
                'sort_order' => '1'
            ),
            array(
                'title'      => '2 year',
                'price'      => 0,
                'price_type' => 'fixed',
                'sku'        => '2_year_warranty',
                'sort_order' => '2'
            ),
            array(
                'title'      => '3 year',
                'price'      => 0,
                'price_type' => 'fixed',
                'sku'        => '3_year_warranty',
                'sort_order' => '3'
            )
        );
    }

}

$shell = new Mage_Shell_Addoptions();
$shell->run();
