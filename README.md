# Warranty Module #

This warranty module is designed to use the custom options for products, the module will add warranty custom options to products that require a warranty. The warranty products will be defined under two separate category types 'large items' and 'small items'. The module will update the warranty options on the defined conditions.

Magento version Enterprise 1.14.2.1

## Installation ##

---
### Assumptions made ###
* The categories 'large items' and 'small items' don't exist.
* Magento is set to run setup scripts automatically

---

* Add the files to the root directory
* Reload magento to allow setup scripts to run
* Login to the admin and go to categories
* Add the products that need the warranty options to the 'large items' and 'small items' categories, it's important to run this before the shell script
* Login to the root of the site via shell and go to ./shell
* Run the shell script, this will set the default warranty options to the above products
```
#!php

php addoptions.php
```
### Overview ###
What happens when the setup scripts run a table is set in the database with the defined options pricing. This is used to update the options prices. The shell script creates the default custom options for the required products

